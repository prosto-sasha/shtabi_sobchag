'use strict';

const gulp          = require('gulp');
const babel         = require('gulp-babel');
const sourcemaps    = require('gulp-sourcemaps');
const concat        = require('gulp-concat');
const sass          = require('gulp-sass');
const cleanCSS      = require('gulp-clean-css');
const minifyJS      = require('gulp-minify');
const remove        = require('gulp-clean');
const csso          = require('gulp-csso');
const rename        = require('gulp-rename');
const mmq           = require('gulp-merge-media-queries');
const runSequence   = require('run-sequence');
const px2rem        = require('gulp-px2rem');
const pug           = require('gulp-pug');
const imagemin      = require('gulp-imagemin');
const mozjpeg       = require('imagemin-mozjpeg');
const pngquant      = require('imagemin-pngquant');
const svgo          = require('imagemin-svgo');
const autoprefixer  = require('gulp-autoprefixer');
const browserSync   = require('browser-sync').create();
const wait          = require('gulp-wait');
const inlinesource  = require('gulp-inline-source');
const replace       = require('gulp-replace');
const htmlmin       = require('gulp-htmlmin');
const jimp          = require("gulp-jimp-resize");

const FOLDER_NAME = 'shtabi_sobchag';
const DATE_PUB = '2017-12-30';

const PATHS = {
  dev: ['_dev/'],
  preprod: ['_preprod/'],
  prod: ['_prod/' + FOLDER_NAME + '/'],
  imgs: 'images/content/**/*.{jpg,png,svg}',
  jsApp: [
    'js/plugins/*.js',
    'js/global.js',
    'js/main.js'
  ],
  jsLibs: [
    'js/libs/promise.js',
    'js/libs/fontfaceobserver.js',
    'js/fonts-load.js',
    'js/libs/feature.js',
    'js/libs/device.js',
    'js/libs/jquery.js'
  ],
  scss: 'scss/app.scss',
  fonts: 'fonts/**/*',
  favicons: 'images/favicons/*',
  pug: ['pug/*.pug', '!pug/includes/**/*.pug']
};

const px2remConfig = {
  replace: true,
  rootValue: 10
};

const imageminConfig = [
  mozjpeg({
    quality: 90,
    progressive: true,
    tune: "ms-ssim",
    smooth: 2
  }),
  pngquant({
    quality: 90,
    speed: 5
  }),
  svgo({
    removeViewBox: false
  })
];

const formatsImgs = [{
    suffix: '2200',
    width: 2200
  },{
    suffix: '1920',
    width: 1920
  },{
    suffix: '1600',
    width: 1600
  },{
    suffix: '1280',
    width: 1280
}];

function getDateNow() {
  const date = new Date();
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  return `${year}-${month < 10 ? '0' + month : month}-${day}`;
}

//------------------------------ JS Concat Tasks

gulp.task('jsAppDev', () => gulp.src(PATHS.jsApp)
  .pipe(sourcemaps.init())
  .pipe(babel())
  .pipe(concat('app.js'))
  .pipe(sourcemaps.write("."))
  .pipe(gulp.dest(PATHS.dev + 'js/'))
);

gulp.task('jsLibsDev', () => gulp.src(PATHS.jsLibs)
  .pipe(concat('libs.js'))
  .pipe(gulp.dest(PATHS.dev + 'js/'))
);

//------------------------------

//------------------------------ Minify JS

gulp.task('minify', () => gulp.src(PATHS.dev + 'js/*.js')
  .pipe(minifyJS({
    noSource: true,
    ext: {
      min:'.min.js'
    }
  }))
  .pipe(gulp.dest(PATHS.prod + 'js/'))
);

gulp.task('minifyPreProd', () => gulp.src(PATHS.dev + 'js/*.js')
  .pipe(minifyJS({
    noSource: true,
    ext: {
      min:'.min.js'
    }
  }))
  .pipe(gulp.dest(PATHS.preprod + 'js/'))
);

//------------------------------

//------------------------------ SASS Compile Tasks

gulp.task('sassDev', () => gulp.src(PATHS.scss)
  .pipe(wait(500))
  .pipe(sourcemaps.init())
  .pipe(sass({
    outputStyle: 'expanded',
    includePaths: require('node-bourbon').includePaths
  }))
  .on('error', sass.logError)
  .pipe(px2rem(px2remConfig))
  .pipe(autoprefixer({
    browsers: ['last 1 version'],
    cascade: false
  }))
  .pipe(sourcemaps.write())
  .pipe(rename('assets.dev.css'))
  .pipe(gulp.dest(PATHS.dev + 'css/'))
  .pipe(browserSync.stream())
);

gulp.task('sassPreProd', () => gulp.src(PATHS.scss)
  .pipe(sass({
    outputStyle: 'compressed',
    includePaths: require('node-bourbon').includePaths
  }))
  .on('error', sass.logError)
  .pipe(px2rem(px2remConfig))
  .pipe(autoprefixer({
    browsers: ['last 2 version', 'ie 10'],
    cascade: false
  }))
  .pipe(rename('assets.min.css'))
  .pipe(mmq())
  .pipe(cleanCSS({
    keepSpecialComments: 0
  }))
  .pipe(csso({
    report: 'gzip'
  }))
  .pipe(gulp.dest(PATHS.preprod + 'css/'))
);

gulp.task('sassProd', () => gulp.src(PATHS.scss)
  .pipe(sass({
    outputStyle: 'compressed',
    includePaths: require('node-bourbon').includePaths
  }))
  .on('error', sass.logError)
  .pipe(px2rem(px2remConfig))
  .pipe(autoprefixer({
    browsers: ['last 2 version', 'ie 10'],
    cascade: false
  }))
  .pipe(rename('assets.min.css'))
  .pipe(mmq())
  .pipe(cleanCSS({
    keepSpecialComments: 0
  }))
  .pipe(csso({
    report: 'gzip'
  }))
  .pipe(gulp.dest(PATHS.prod + 'css/'))
);

//------------------------------

//------------------------------ Remove Folders

gulp.task('cleanDev', () => gulp.src([
  PATHS.dev + 'css',
  PATHS.dev + 'fonts',
  PATHS.dev + 'images/favicons',
  PATHS.dev + 'js',
  PATHS.dev + 'index.html'
], {read: false}).pipe(remove()));

gulp.task('cleanProd', () => gulp.src([
  PATHS.prod + 'fonts',
  PATHS.prod + 'images/favicons',
  PATHS.prod + 'js',
  '_prod/*.html'
], {read: false}).pipe(remove()));

gulp.task('cleanPreProd', () => gulp.src([
  PATHS.preprod + 'fonts',
  PATHS.preprod + 'images/favicons',
  PATHS.preprod + 'js',
  PATHS.preprod + 'index.html'
], {read: false}).pipe(remove()));

gulp.task('cleanImgDev', () => gulp.src(PATHS.dev + 'images/content', {read: false}).pipe(remove()));
gulp.task('cleanImgPreProd', () => gulp.src(PATHS.preprod + 'images/content', {read: false}).pipe(remove()));
gulp.task('cleanImgProd', () => gulp.src(PATHS.prod + 'images/content', {read: false}).pipe(remove()));

gulp.task('cleanFullDev', () => gulp.src('_dev', {read: false}).pipe(remove()));
gulp.task('cleanFullPreProd', () => gulp.src('_preprod', {read: false}).pipe(remove()));
gulp.task('cleanFullProd', () => gulp.src('_prod', {read: false}).pipe(remove()));

gulp.task('deleteProd', () => gulp.src([PATHS.prod + '*.html', PATHS.prod + 'css'], {read: false}).pipe(remove()));
gulp.task('deletePreProd', () => gulp.src([PATHS.preprod + 'css'], {read: false}).pipe(remove()));

//------------------------------

//------------------------------ Copy Files Tasks

gulp.task('copyDevFonts', () => gulp.src(PATHS.fonts).pipe(gulp.dest(PATHS.dev + 'fonts/')));
gulp.task('copyPreProdFonts', () => gulp.src(PATHS.fonts).pipe(gulp.dest(PATHS.preprod + 'fonts/')));
gulp.task('copyProdFonts', () => gulp.src(PATHS.fonts).pipe(gulp.dest(PATHS.prod + 'fonts/')));

gulp.task('copyDevFavicons', () => gulp.src(PATHS.favicons).pipe(gulp.dest(PATHS.dev + 'images/favicons/')));
gulp.task('copyPreProdFavicons', () => gulp.src(PATHS.favicons).pipe(gulp.dest(PATHS.preprod + 'images/favicons/')));
gulp.task('copyProdFavicons', () => gulp.src(PATHS.favicons).pipe(gulp.dest(PATHS.prod + 'images/favicons/')));

//------------------------------

//------------------------------ ImageMin Tasks

gulp.task('imageMinDev', () => gulp.src(PATHS.imgs)
  .pipe(jimp({
    sizes: formatsImgs
  }))
  .pipe(imagemin(imageminConfig))
  .pipe(gulp.dest(PATHS.dev + 'images/content/'))
);

gulp.task('imageMinPreProd', () => gulp.src(PATHS.imgs)
  .pipe(jimp({
    sizes: formatsImgs
  }))
  .pipe(imagemin(imageminConfig))
  .pipe(gulp.dest(PATHS.preprod + 'images/content/'))
);

gulp.task('imageMinProd', () => gulp.src(PATHS.imgs)
  .pipe(jimp({
    sizes: formatsImgs
  }))
  .pipe(imagemin(imageminConfig))
  .pipe(gulp.dest(PATHS.prod + 'images/content/'))
);

//------------------------------

//------------------------------ Pug Compile Tasks

gulp.task('pugDev', () => gulp.src(PATHS.pug)
  .pipe(pug({
    pretty: true,
    data: {
      env: 'dev',
      embed: false,
      folderName: FOLDER_NAME,
      datePub: DATE_PUB,
      dateModify: getDateNow()
    }
  }))
  .pipe(gulp.dest('_dev'))
);

gulp.task('pugPreProd', () => gulp.src(PATHS.pug)
  .pipe(pug({
    pretty: true,
    data: {
      env: 'preprod',
      embed: false,
      folderName: FOLDER_NAME,
      datePub: DATE_PUB,
      dateModify: getDateNow()
    }
  }))
  .pipe(gulp.dest('_preprod'))
);

gulp.task('pugProd', () => gulp.src(PATHS.pug)
  .pipe(pug({
    pretty: true,
    data: {
      env: 'prod',
      embed: false,
      folderName: FOLDER_NAME,
      datePub: DATE_PUB,
      dateModify: getDateNow()
    }
  }))
  .pipe(gulp.dest('_prod/' + FOLDER_NAME))
);

gulp.task('pugEmbed', () => gulp.src(PATHS.pug)
  .pipe(pug({
    pretty: true,
    data: {
      env: 'prod',
      embed: true,
      folderName: FOLDER_NAME
    }
  }))
  .pipe(gulp.dest('_prod/' + FOLDER_NAME))
);

//------------------------------

//------------------------------ Include JS CSS IMG Task

gulp.task('inlineDev', () => gulp.src('_dev/*.html')
  .pipe(inlinesource({
    compress: false,
    pretty: true
  }))
  .pipe(gulp.dest('_dev'))
);

gulp.task('inlinePreProd', () => gulp.src('_preprod/*.html')
  .pipe(inlinesource({
    compress: false,
    pretty: true
  }))
  .pipe(htmlmin({collapseWhitespace: true, removeComments: true, minifyJS: true}))
  .pipe(gulp.dest('_preprod'))
);

gulp.task('inlineProd', () => gulp.src('_prod/' + FOLDER_NAME + '/*.html')
  .pipe(inlinesource({
    compress: false,
    pretty: true
  }))
  .pipe(rename(FOLDER_NAME + ".html"))
  .pipe(replace('fonts/', '/special_assets/' + FOLDER_NAME + '/fonts/'))
  .pipe(htmlmin({collapseWhitespace: true, removeComments: true, minifyJS: true}))
  .pipe(gulp.dest('_prod'))
);

gulp.task('inlineEmbed', () => gulp.src('_prod/' + FOLDER_NAME + '/*.html')
  .pipe(inlinesource({
    compress: false
  }))
  .pipe(rename(FOLDER_NAME + ".embed.html"))
  .pipe(replace('fonts/', '/special_assets/' + FOLDER_NAME + '/fonts/'))
  .pipe(htmlmin({collapseWhitespace: true, removeComments: true, minifyJS: true}))
  .pipe(gulp.dest('_prod'))
);

//------------------------------

//------------------------------ Watch

gulp.task('watch', () => {
  gulp.watch('pug/**/*.pug', () => runSequence('pugDev', 'inlineDev'));
  gulp.watch('scss/**/*.scss', () => runSequence('sassDev', 'pugDev', 'inlineDev'));
  gulp.watch('js/**/*.js', ['jsAppDev', 'jsLibsDev']);
  gulp.watch('images/**/*.{png,jpg,svg}', ['cleanImgDev', 'imageMinDev']);
});

//------------------------------

//------------------------------ BrowserSync

gulp.task('brSync', () => {
  browserSync.init({
    server: {
      watchTask: true,
      baseDir: "_dev/"
    },
    port: 7788,
    open: "external"
  });

  gulp.watch('_dev/*.html').on('change', browserSync.reload);
  gulp.watch('_dev/**/*.js').on('change', browserSync.reload);
  gulp.watch('_dev/images/**/*.{png,jpg,svg}').on('change', browserSync.reload);
});

//------------------------------

gulp.task('default', ['brSync', 'watch']);

gulp.task('dev', () => runSequence(
  'cleanDev',
  'sassDev',
  ['jsAppDev','jsLibsDev'],
  'pugDev',
  'inlineDev',
  ['copyDevFonts','copyDevFavicons']
));

gulp.task('preprod', () => runSequence(
  'cleanPreProd',
  ['jsAppDev','jsLibsDev'],
  'minifyPreProd',
  'pugPreProd',
  'sassPreProd',
  'inlinePreProd',
  ['copyPreProdFonts','copyPreProdFavicons'],
  'deletePreProd'
));

gulp.task('prod', () => runSequence(
  'cleanProd',
  ['jsAppDev','jsLibsDev'],
  'minify',
  'pugProd',
  'sassProd',
  'inlineProd',
  ['copyProdFonts','copyProdFavicons'],
  'deleteProd'
));

gulp.task('embed', () => runSequence(
  'cleanProd',
  ['jsAppDev','jsLibsDev'],
  'minify',
  'pugEmbed',
  'sassProd',
  'inlineEmbed',
  ['copyProdFonts','copyProdFavicons'],
  'deleteProd'
));

gulp.task('imgDev', () => runSequence(
  'cleanImgDev',
  'imageMinDev'
));

gulp.task('imgPreProd', () => runSequence(
  'cleanImgPreProd',
  'imageMinPreProd'
));

gulp.task('imgProd', () => runSequence(
  'cleanImgProd',
  'imageMinProd'
));