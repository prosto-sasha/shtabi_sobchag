'use strict';

const HTML = document.documentElement;
const BODY = document.body;
const PAGE_URL = parseURL(window.location.href);
const PAGE_LINK = `${PAGE_URL.protocol}://${PAGE_URL.host}${PAGE_URL.path}${PAGE_URL.hash !== '' ? '#' + PAGE_URL.hash : ''}`;
const linkPage = document.querySelector('#isLinkArticle');
const isMobile = device.mobile();
const isIOS = device.ios();

// Remove class 'no-js'
BODY.classList.remove('no-js');

// Features Detect Support
feature.touch ? HTML.classList.add('touch') : HTML.classList.add('no-touch');

// Detect iOS
isIOS ? HTML.classList.add('is-ios') : '';

// Link Article Print & Meta
linkPage.innerHTML = PAGE_LINK;

if (!isEmbed) {
  const metaLinkArticle = document.querySelector('[itemprop="mainEntityOfPage"]');
  metaLinkArticle.href = PAGE_LINK;
}

// Lazy Load Media (Images, Iframes, Scripts, Video, Audio)
const blazy = new Blazy({
  selector: '.is-lazy-load',
  successClass: 'has-loaded',
  offset: 1000,
  breakpoints: [{
    width: 1280, src: 'data-src-1280'
  },{
    width: 1600, src: 'data-src-1600'
  },{
    width: 1920, src: 'data-src-1920'
  }]
});

window.addEventListener('resize', () => blazy.revalidate());

// Render Notes List
const authorSection = $('.b-section--author');
const supList = $('sup[data-content]');
const noteListWrap = $('<section>').addClass('b-section b-section--note-list').attr('hidden', '');
const noteTitle = $('<div>').addClass('b-title').text('Примечания:');
const noteListUl = $('<ul>').addClass('b-note-list__items');

supList.each((index, item) => {
  const noteListLi = $('<li>').addClass('b-note-list__item');
  noteListLi.html($(item).data('content'));
  noteListUl.append(noteListLi);
});

noteListWrap.append(noteTitle);
noteListWrap.append(noteListUl);
noteListWrap.insertBefore(authorSection);

// Add listener beforeprint
//-- WebKit
window.matchMedia('print').addListener(mql => {
  if (mql.matches) {
    beforePrint();
  }
});

//-- Other Browsers
window.onbeforeprint = beforePrint();

// Parse URL
function parseURL(url) {
  const A = document.createElement('a');
  A.href = url;
  return {
    source: url,
    protocol: A.protocol.replace(':',''),
    host: A.hostname,
    port: A.port,
    query: A.search,
    params: (function () {
      const ret = {};
      const seg = A.search.replace(/^\?/,'').split('&');
      const len = seg.length;
      let i = 0;
      let s;
      for ( ; i<len ; i++) {
        if (!seg[i]) {
          continue;
        }
        s = seg[i].split('=');
        ret[s[0]] = s[1];
      }
      return ret;
    })(),
    file: (A.pathname.match(/\/([^\/?#]+)$/i) || [,''])[1],
    hash: A.hash.replace('#',''),
    path: A.pathname.replace(/^([^\/])/,'/$1'),
    relative: (A.href.match(/tps?:\/\/[^\/]+(.+)/) || [,''])[1],
    segments: A.pathname.replace(/^\//,'').split('/')
  };
}

// Do something before print page
function beforePrint() {
  const bLazyPrint = new Blazy();
  bLazyPrint.load($('.is-lazy-load:not(iframe)'), true);
}