var html = document.documentElement;

// Roboto
var Roboto400 = new FontFaceObserver("Roboto", {
  weight: 400
});

// PT Serif
var PTSerif400 = new FontFaceObserver("PT Serif", {
  weight: 400
});

var PTSerif400Italic = new FontFaceObserver("PT Serif", {
  weight: 400,
  style: "italic"
});

var PTSerif700 = new FontFaceObserver("PT Serif", {
  weight: 700
});

var PTSerif700Italic = new FontFaceObserver("PT Serif", {
  weight: 700,
  style: "italic"
});

html.classList.add('is-fonts-loading');

Promise.all([
  Roboto400.load(),
  PTSerif400.load(),
  PTSerif400Italic.load(),
  PTSerif700.load(),
  PTSerif700Italic.load()
]).then(function() {
  html.classList.remove('is-fonts-loading');
  html.classList.add('is-fonts-loaded');
}).catch(function () {
  html.classList.remove('is-fonts-loading');
  html.classList.add('is-fonts-failed');
});

