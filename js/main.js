;(function (){

  // Modal Notes
  const modalNote = $('[data-remodal-id="modalNote"]').remodal({
    hashTracking: false
  });

  $('.b-section--text sup').on('click', function (e) {
    e.preventDefault();
    $('#modalTextNote, #modalNumberNote').text(' ');
    $('#modalTextNote').html($(this).data('content'));
    $('#modalNumberNote').text($(this).text());
    modalNote.open();
  });

  // Scroll To
  $('[data-to-id]').on('click', function (e) {
    e.preventDefault();
    const id = $(this).data('to-id');
    $('html, body').animate({
      scrollTop: $('#' + id).offset().top
    }, 700);
  });

})();
